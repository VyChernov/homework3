package ru.task2;

public class Vector3 {
    private double x;
    private double y;
    private double z;

    /**
     * Create new Vector3 instance
     *
     * @param x X value of vector
     * @param y Y value of vector
     * @param z Z value of vector
     */
    public Vector3(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * @return X of this vector
     */
    public double getX() {
        return x;
    }

    /**
     * @return Y of this vector
     */
    public double getY() {
        return y;
    }

    /**
     * @return Z of this vector
     */
    public double getZ() {
        return z;
    }

    /**
     * @return Vector length
     */
    public double length() {
        return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
    }

    /**
     * Get scalar multiplication of two vectors
     *
     * @param vector second vector
     * @return scalar multiplication of two vectors
     */
    public double scalar(Vector3 vector) {
        return (this.x * vector.getX()) + (this.y * vector.getY()) + (this.z * vector.getZ());
    }

    /**
     * Get multiplication of two vectors
     *
     * @param vector second vector
     * @return multiplication of two vectors
     */
    public Vector3 vectorMpl(Vector3 vector) {
        return new Vector3(
                this.y * vector.getZ() - this.z * vector.getY(),
                this.z * vector.getX() - this.x * vector.getZ(),
                this.x * vector.getY() - this.y * vector.getX()
        );
    }
}
