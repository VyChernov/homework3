package ru.task1.Primitives;

public class Parallelepiped extends Square {
    /**
     * Create new parallelepiped
     *
     * @param name       Parallelepiped name
     * @param edgeLength Parallelepiped edge length
     */
    public Parallelepiped(String name, double edgeLength) {
        super(name, edgeLength);
    }

    /**
     * Create new parallelepiped
     *
     * @param name Parallelepiped name
     */
    public Parallelepiped(String name) {
        super(name);
    }

    /**
     * Create new Parallelepiped
     *
     * @param edgeLength Square edge length
     */
    public Parallelepiped(double edgeLength) {
        super(edgeLength);
    }

    /**
     * @return Get parallelepiped volume
     */
    public double getVolume() {
        return Math.pow(this.getEdgeLength(), 3);
    }


}
