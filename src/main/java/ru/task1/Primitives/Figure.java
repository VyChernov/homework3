package ru.task1.Primitives;

public class Figure {
    private final String name;
    private double area;

    /**
     * Create new figure
     *
     * @param name Figure name
     * @param area Figure area
     */
    public Figure(String name, double area) {
        this.name = name;
        this.area = area;
    }

    /**
     * Create new figure
     *
     * @param name Figure name
     */
    public Figure(String name) {
        this(name, 0);
    }

    /**
     * Create new figure
     *
     * @param area Figure area
     */
    public Figure(double area) {
        this("Figure", area);
    }


    /**
     * @return Figure area
     */
    public double getArea() {
        return area;
    }

    /**
     * @return Figure name
     */
    public String getName() {
        return name;
    }
}
