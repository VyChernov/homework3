package ru.task1.Primitives;

public class Square extends Figure {

    private double edgeLength;

    /**
     * Create new square
     *
     * @param name       Square name
     * @param edgeLength Square edge length
     */
    public Square(String name, double edgeLength) {
        super(name, edgeLength * edgeLength);
        this.edgeLength = edgeLength;
    }

    /**
     * Create new square
     *
     * @param name Square name
     */
    public Square(String name) {
        super(name);
        this.edgeLength = 0;
    }

    /**
     * Create new square
     *
     * @param edgeLength Square edge length
     */
    public Square(double edgeLength) {
        super(edgeLength * edgeLength);
        this.edgeLength = edgeLength;
    }

    /**
     * @param edgeLength New edge length
     */
    public void setEdgeLength(double edgeLength) {
        this.edgeLength = edgeLength;
    }

    /**
     * @return Edge length value
     */
    public double getEdgeLength() {
        return edgeLength;
    }

    /**
     * @return Square area
     */
    @Override
    public double getArea() {
        return this.edgeLength * this.edgeLength;
    }
}
