package ru.task1.Primitives;

public class Circle extends Figure {
    private double radius;

    /**
     * Create new circle
     *
     * @param name circle name
     * @param radius circle area
     */
    public Circle(String name, double radius) {
        super(name, Math.PI * (radius * radius));
        this.radius = radius;
    }

    /**
     * Create new circle
     *
     * @param name circle name
     */
    public Circle(String name) {
        super(name);
        this.radius = 0;
    }

    /**
     * Create new circle
     *
     * @param radius circle area
     */
    public Circle(double radius) {
        super(Math.PI * (radius * radius));
        this.radius = radius;
    }

    /**
     * @param radius New circle radius
     */
    public void setRadius(double radius) {
        this.radius = radius;
    }

    /**
     * @return Circle area
     */
    @Override
    public double getArea() {
        return Math.PI * (this.radius * this.radius);
    }
}
