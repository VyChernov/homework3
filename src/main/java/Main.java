import ru.task1.Primitives.*;
import ru.task2.Vector3;


public class Main {

    /**
     * Realisation of task one
     */
    public static void runTaskOne() {
        System.out.print("=== Task one ===\n\n");

        Figure[] figures = {
                new Figure("Figure Prototype", 10.54),
                new Square("Simple Square", 4),
                new Circle("Simple Circle", 2),
                new Parallelepiped("Parallelepiped(or cube :) )", 6)
        };

        for (Figure fig : figures) {
            System.out.printf("name: %s, area: %f", fig.getName(), fig.getArea());

            if (fig instanceof Parallelepiped) {
                System.out.printf(", volume: %f \n", ((Parallelepiped) fig).getVolume());
            } else {
                System.out.print("\n");
            }
        }

        System.out.print("\n\n");
    }

    /**
     * Realisation of task two
     */
    public static void runTaskTwo() {
        System.out.print("=== Task two ===\n\n");

        Vector3 v1 = new Vector3(2, 5.4, 1);
        Vector3 v2 = new Vector3(10, 7.7, 10.1);

        System.out.printf("v1 length: %f\n", v1.length());
        System.out.printf("scalar mpl v1 and v2: %f\n", v1.scalar(v2));

        Vector3 v3 = v1.vectorMpl(v2);
        System.out.printf("v3(v1*v2) -  x:%f y:%f z:%f\n", v3.getX(), v3.getY(), v3.getZ());
        System.out.printf("v3 length: %f\n", v1.length());

        System.out.print("\n\n");
    }

    public static void main(String[] args) {
        runTaskOne();
        runTaskTwo();
    }
}
